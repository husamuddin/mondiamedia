const {PORT} = require('./config/env')
const path = require('path')

const mognooseConfig = require('./config/mongoose')

const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const validator = require('express-validator')
const cors = require('cors')

const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')

const errorHandling = require('./config/errorHandling')
const responses = require('./api/responses')
const jwtConfig = require('./config/jwt').config
const routes = require('./config/routes')

// loading responses
app.use(responses)

// setting up the app
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(validator())

// setting up swagger
app.use(
    '/swagger',
    swaggerUi.serve,
    swaggerUi.setup(swaggerDocument)
)

// cors enable (THIS ISN'T THE GREATEST WAY, BUT THIS IS JUST FOR SIMPLISITY)
app.use(cors())

// serve static assets
app.use(
    express.static(
        path.resolve(
            __dirname, 'public'
        )
    )
)

// jwt setting up
app.use(jwtConfig.initialize())

// setting up mongodb
mognooseConfig()

// api
app.use('/api', routes)

app.use('/*', express.static(
    path.resolve(
        __dirname, 'public'
    )
))


// setting up error handlers
errorHandling(app)

app.listen(PORT)

