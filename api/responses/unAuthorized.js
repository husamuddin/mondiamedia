const defaultError = {
    'Name': 'NotAuthorized',
    'message': 'not authorized'
}

module.exports = function(error = defaultError) {
    return this.status(401).send({
        error
    })
}
