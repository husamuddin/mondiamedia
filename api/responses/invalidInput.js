const defaultError = {
    'Name': 'InvalidInput',
    'message': 'invalid input'
}

module.exports = function(error = defaultError) {
    return this.status(403).send({
        error
    })
}
