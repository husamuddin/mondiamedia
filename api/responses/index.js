const express = require('express')

const Responses = express()

Responses.use((req, res, next) => {
    const responses = [
        'ok',
        'invalidInput',
        'notFound',
        'unAuthorized',
        'serverError',
    ]

    responses.forEach(
        (response) => {
            res[response] = require(`./${response}`).bind(res)
        }
    )

    next()
})

module.exports = Responses
