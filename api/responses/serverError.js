const defaultError = {
    'Name': 'ServerError',
    'message': 'server error'
}

module.exports = function(error = defaultError) {
    if(error.constructor === Object && !Object.keys(error).length) {
        error = defaultError
    }

    return this.status(500).send({
        error
    })
}
