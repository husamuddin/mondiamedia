const defaultError = {
    'Name': 'PageNotFound',
    'message': 'page not found'
}

module.exports = function(error = defaultError) {
    return this.status(404).send({
        error
    })
}
