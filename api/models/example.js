const mongoose = require('mongoose')
const {Schema} = mongoose

module.exports = mongoose.model('language',
    new Schema({
        name: String,
        title: String,
        introduction: String,
    })
)

