const mongoose = require('mongoose')
const {Schema} = mongoose

module.exports = mongoose.model('language',
    new Schema({
        name: {
            type: String,
            unique: true,
            index: true,
            required: true
        },
        title: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        introduction: {
            type: String,
            required: true
        },
        example: {
            type: String,
            required: true
        },
        lessons: {
            type: [
                {
                    type: Schema.Types.ObjectId, ref: 'lesson',
                }
            ],
            required: true
        }
    })
)

