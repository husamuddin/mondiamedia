const mongoose = require('mongoose')
const {Schema} = mongoose

module.exports = mongoose.model('lesson',
    new Schema({
        name: {
            type: String,
            index: true,
            unique: true,
        },
        body: String,
        examples: Array,
        language: {
            type: Schema.Types.ObjectId, ref: 'language',
            required: true
        }
    })
)
