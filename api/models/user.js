const mongoose = require('mongoose')
const {Schema} = mongoose

module.exports = mongoose.model('user',
    new Schema({
        username: {
            type: String,
            index: true,
            unique: true,
        },
        hash: String,
    })
)
