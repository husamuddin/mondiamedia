const User = require('../models/user')

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const {SALT_ROUNDS, SECRET} = require('../../config/env')

exports.generatePasswordHash = (password) => {
    return bcrypt.hashSync(password, SALT_ROUNDS)
}

exports.generateJwtToken = (payload) => {
   return jwt.sign(
        payload,
        SECRET,
        { expiresIn: '1h' }
    )
}
