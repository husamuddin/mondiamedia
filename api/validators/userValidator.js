module.exports = {
    create: (req, res, next) => {
        req.check('username', `username parameter is required`).exists()
        req.check('username', `username parameter can't be empty`).notEmpty()

        req.check('password', `name parameter is required`).exists()
        req.check('password', `name parameter can't be empty`).notEmpty()

        console.log(
            req.getValidationResult().then(
                (errors) => {
                    if(errors.isEmpty()) {
                        next()
                    } else {
                        next(errors.array({onlyFirstError: true}))
                    }
                }
            )
        )
    }
}
