module.exports = {
    create: (req, res, next) => {
        req.check('title', `title parameter is required`).exists()
        req.check('title', `title parameter can't be empty`).notEmpty()

        req.check('name', `name parameter is required`).exists()
        req.check('name', `name parameter can't be empty`).notEmpty()

        console.log(
            req.getValidationResult().then(
                (errors) => {
                    if(errors.isEmpty()) {
                        next()
                    } else {
                        next(errors.array({onlyFirstError: true}))
                    }
                }
            )
        )
    }
}
