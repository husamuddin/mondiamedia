const Language = require('../models/language')

module.exports = {
    read(req, res, next) {
        return Language.find({}, (error, docs) => {
            if(error) return next(error)

            return res.ok(docs)
        })
    },
    create(req, res, next) {
        const { title, name, description, introduction, example } = req.body

        const language = new Language(
            { title, name, description, introduction, example }
        )

        language.save(
            (error, doc) => {
                if(error) return next(error)

                return res.ok(doc)
            }
        )
    },
    update(req, res, next) {
        const { languageId } = req.params

        const {
            name,
            title,
            description,
            introduction,
            example
        } = req.body

        let updates = {}

        if(name)
            updates['name'] = name

        if(title)
            updates['title'] = title

        if(description)
            updates['description'] = description

        if(introduction)
            updates['introduction'] = introduction

        if(example)
            updates['example'] = example

        return Language.findOneAndUpdate(
            languageId,
            {
                $set: updates
            },
            {
                new: true
            }, (error, doc) => {
                if(error) return next(error)

                return res.ok(doc)
            }
        )
    },

    delete(req, res, next) {
        const { languageId } = req.params

        return Language.findOneAndRemove(languageId, (error, doc) => {
            if(error) return next(error)

            return res.ok(doc)
        })
    }
}
