const User = require('../models/user')
const userService = require('../services/userService')
const jwt = require('jsonwebtoken')
const { USER1_USERNAME, USER1_PASSWORD } = require('../../config/env')

module.exports = {
    read(req, res, next) {
        User.find({}, {username: true, _id: true}, (error, docs) => {
            if(error) return next(error)

            return res.ok(docs)
        })
    },

    create(req, res, next) {
        const {username, password} = req.body

        const hash = userService.generatePasswordHash(password)

        let user = new User({
            username,
            hash
        })

        user.save(
            (error, doc) => {
                if(error) return next(error)

                return res.ok(doc)
            }
        )
    },

    update(req, res, next) {
        let updates = {}
        const {username, password} = req.body

        if(username)
            updates['username'] = username

        if(password) {
            updates['hash'] = userService.generatePasswordHash(password)
        }

        User.findOneAndUpdate(
            {id: req.id},
            { $set: updates },
            { new: true },
            (error, doc) => {
                if(error) return next(error)

                return res.ok(doc)
            }
        )
    },

    delete(req, res) {
        const { id } = req.params

        User.findOneAndRemove(id, (error, doc) => {
            if(error) return next(error)

            return res.ok(doc)
        })
    },

    authenticate(req, res) {
        const {user} = req
        const {name, password} = req.body

        const token = userService.generateJwtToken(
            {
                user: {
                    id: user.id,
                    name: user.name
                }
            }
        )

        return res.send({token})
    },

    setup(req, res, next) {
        const user1 = new User({
            username: USER1_USERNAME,
            hash: userService.generatePasswordHash(USER1_PASSWORD)
        })

        user1.save((error) => {
            if(!error || error.name === 'BulkWriteError')
                return res.ok({message: 'done'})

            if(error) return next(error)
        })
    }
}

