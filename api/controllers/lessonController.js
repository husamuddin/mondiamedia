const Lesson = require('../models/lesson')
const Language = require('../models/language')

module.exports = {
    read(req, res, next) {
        return Lesson.find({}).exec(
            (error, docs) => {
                if(error) return next(errors)

                res.ok(docs)
            }
        )
    },
    create(req, res, next) {
        const { name, body, examples, languageId } = req.body

        return Language.findById(languageId, (error, language) => {
            if(error) return next(error)
            if(!language) return({
                name: 'NoLanguageFound',
                message: 'no language found'
            })

            return Lesson.create(
                { name, body, examples, language: languageId },
                (error, lesson) => {
                    if(error) return next(error)

                    if( language.lessons.indexOf(lesson._id) === -1 ) {
                        language.lessons.push(lesson._id)

                        return language.save(
                            (error) => {
                                if(error) return next(error)

                                return res.ok(lesson)
                            }
                        )
                    }

                    return res.ok(lesson)
                }
            )

        })

    },
    update(req, res, next) {
        const { lessonId } = req.params

        const { name, body, examples, languageId } = req.body

        let updates = {}

        if(name)
            updates['name'] = name

        if(body)
            updates['body'] = body

        if(examples && !!examples.length)
            updates['examples'] = examples

        if(languageId) {
            updates['languageId'] = languageId
            return Language.findById(languageId, (error, language) => {
                if(error) return next(error)
                if(!language) return next({
                    name: 'LanguageNotFound',
                    message: 'language not found'
                })

                return Lesson.findOneAndUpdate(
                    lessonId,
                    {
                        $set: updates
                    },
                    {
                        new: true
                    }, (error, lesson) => {
                        if(error) return next(error)

                        if(!language.lessons.indexOf(lesson._id)) {
                            language.lessons.push(lesson._id)

                            return language.save(
                                (error) => {
                                    return res.ok(lesson)
                                }
                            )
                        }

                        return res.ok(lesson)
                    }
                )
            })
        } else {
            return Lesson.findOneAndUpdate(
                lessonId,
                {
                    $set: updates
                },
                {
                    new: true
                }, (error, doc) => {
                    if(error) return next(error)

                    return res.ok(doc)
                }
            )
        }
    },

    delete(req, res, next) {
        const { lessonId } = req.params

        return Lesson.findOneAndRemove(lessonId, (error, doc) => {
            if(error) return next(error)

            return res.ok(doc)
        })
    }
}
