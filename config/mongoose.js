const {DATABASE} = require('./env')
const mongoose = require('mongoose')

module.exports = () => {
    mongoose.Promise = global.Promise
    mongoose.connect(DATABASE)

    mongoose.connection.on(
        'error',
        console.error.bind(console, 'mongodb connection error:')
    )

    mongoose.connection.once('open', function() {
        console.log(`mongodb is connected`)
    })
}

