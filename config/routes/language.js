const routes = require('express')()

const languageController = require('../../api/controllers/languageController')
const languageValidator = require('../../api/validators/languageValidator')
const { jwtStrategyMiddleware } =  require('../jwt')

routes.get('/:languageName?', languageController.read)

routes.use(jwtStrategyMiddleware)

routes.post('/', languageController.create)
routes.patch('/:languageId', languageController.update)
routes.delete('/:language', languageController.delete)

module.exports = routes

