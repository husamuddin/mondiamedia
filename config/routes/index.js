const express = require('express')
const userController = require('../../api/controllers/userController')
const language = require('./language')
const user = require('./user')
const lesson = require('./lesson')

const routes = express()

routes.all('/', (req, res) => {
    return res.ok({
        message: `Mondia Task RESTful API`
    })
})

routes.get('/setup', userController.setup)

routes.use('/language', language)
routes.use('/user', user)
routes.use('/lesson', lesson)

routes.all('*', (req, res) => {
    return res.notFound()
})

module.exports = routes

