const routes = require('express')()
const passport = require('passport')

const userController = require('../../api/controllers/userController')
const userValidator = require('../../api/validators/userValidator')
const { localStrategyMiddleware, jwtStrategyMiddleware } =  require('../jwt')

routes.post('/authenticate',
    localStrategyMiddleware,
    userController.authenticate
)

routes.use(jwtStrategyMiddleware)

routes.get('/', userController.read)
routes.post('/',
    userValidator.create,
    userController.create
)
routes.patch('/:id', userController.update)
routes.delete('/:id', userController.delete)

module.exports = routes
