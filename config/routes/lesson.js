const express = require('express')
const routes = express()
const { jwtStrategyMiddleware } =  require('../jwt')
const lessonController = require('../../api/controllers/lessonController')

routes.get('/:lessonName?', lessonController.read)

routes.use(jwtStrategyMiddleware)

routes.post('/', lessonController.create)
routes.patch('/:lessonId', lessonController.update)
routes.delete('/:lessonId', lessonController.delete)

routes.use(jwtStrategyMiddleware)

module.exports = routes
