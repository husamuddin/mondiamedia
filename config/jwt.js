const {SECRET} = require('./env')

const User = require('../api/models/user')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const passport = require("passport")
const LocalStrategy = require('passport-local').Strategy
const passportJWT = require("passport-jwt")

const ExtractJwt = passportJWT.ExtractJwt
const JwtStrategy = passportJWT.Strategy

let localStrategy = new LocalStrategy(
    {
        usernameField: 'username',
        passwordField: 'password'
    },
    (username, password, done) => {
        User.findOne({username}, (error, user) => {
            if(error) done(error)

            if(!user) {
                return done(null, false, {name: 'UserNotFound', message: `user not found`})
            }

            const correctPassword = bcrypt.compareSync(
                password,
                user.hash
            )

            if(!correctPassword) {
                return done(null, false, `incorrect password`)
            }

            return done(null, user)
        })
    }
)

let jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt')
jwtOptions.secretOrKey = SECRET

let jwtStrategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
    User.findOne({_id: jwt_payload.user.id}, (error, user) => {
        if(error) throw error

        if (user) {
            next(null, user)
        } else {
            next(null, false)
        }
    })
})

passport.use(localStrategy)
passport.use(jwtStrategy)

exports.config = passport
exports.localStrategyMiddleware = (req, res, next) => {
    return passport.authenticate('local', { session: false }, (error, user, info) => {
        if(!user) return next(info)

        req.user = user

        return next()
    })(req, res, next)
}
exports.jwtStrategyMiddleware = (req, res, next) => {
    passport.authenticate('jwt', {session: false}, (error, user, info) => {
        if(!user) {
            if(info && info.name === 'Error') {
                next({
                    name: 'JWTNotPresent',
                    message: 'missing jwt token'
                })

                return next(info || {
                    name: 'UserNotFound',
                    message: 'user not found'
                })
            }
        }

        return next()
    })(req, res, next)
}
