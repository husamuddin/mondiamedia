module.exports = {
    PORT: 3000,
    SALT_ROUNDS: 10,
    SECRET: process.env.SECRET || 'loremipsum',
    DATABASE: process.env.DATABASE || 'mongodb://localhost',
    USER1_USERNAME: process.env.USER1_USERNAME || 'admin',
    USER1_PASSWORD: process.env.USER1_PASSWORD || 'admin',
}
