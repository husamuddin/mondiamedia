module.exports = (app) => {
    app.use((error, req, res, next) => {
        if(error.name !== 'JsonWebTokenError')
            return next(error)

        return res.unAuthorized(error)
    })

    app.use((error, req, res, next) => {
        if(error.name !== 'UserNotFound')
            return next(error)

        return res.notFound(error)
    })

    app.use((error, req, res, next) => {
        return res.serverError(error)
    })
}
