# Mondimedia Task
> Please notice that, this project is partially completed. as I'm crowded with
> other tasks I really had little time to work on it. I'm sorry for that.
> but anyway, I've built the skeleton of it, so we have here the main thing, and
> for the rest are just a follow up along.
> 

## Used Technologies:
- Express Nodejs framework
- JWT Authentication
- MongoDB Engine
- Webpack for bundling
- React Library
- Redux for handling the app state
- Swagger for APIs DOCs


## Runing The App
You can run the app by running the following command
```bash
$ docker-compose up -d
```

The application will be accessable on port `:3000`
To seed user1, hit `GET /api/user/setup`, then`POST /api/user/authenticate` with admin/admin for username and password to get the JWT token.
```bash
$ curl -X POST \
	-H "Content-Type: application/json" \
    -d '{"username": "admin","password":"admin"}' \
    http://localhost:3000/api/user/authenticate

```


[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/801db435452e37ca7b93#?env%5Bmondiamedia%20task%5D=W3siZW5hYmxlZCI6dHJ1ZSwia2V5Ijoib3JpZ2luIiwidmFsdWUiOiJodHRwOi8vMTI3LjAuMC4xOjMwMDAvYXBpIiwidHlwZSI6InRleHQifSx7ImVuYWJsZWQiOnRydWUsImtleSI6InVzZXJJZCIsInZhbHVlIjoiNWE3Yzg1NDBiZTk4MjgxOTlmNTFlNzg2IiwidHlwZSI6InRleHQifSx7ImVuYWJsZWQiOnRydWUsImtleSI6InZhcmlhYmxlX2tleSIsInZhbHVlIjoiZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SjFjMlZ5SWpwN0ltbGtJam9pTldFM1l6Y3daRFF4TURaa01EWXdaVFUxTmpNek1UTTFJaXdpYm1GdFpTSTZJblZ6WlhJaWZTd2lhV0YwSWpveE5URTRNVEEzTURJMkxDSmxlSEFpT2pFMU1UZ3hNVEEyTWpaOS41MnZ0Z3dWZUlEUHVwNzBhcnFZeXBHZGN2dWlNeGJBTUMtU05lbVlLSnhNIiwidHlwZSI6InRleHQifSx7ImVuYWJsZWQiOnRydWUsImtleSI6Imxhbmd1YWdlSWQiLCJ2YWx1ZSI6IjVhN2U2OTIxNWRkY2JhMDlmNDBkMGE3MyIsInR5cGUiOiJ0ZXh0In0seyJlbmFibGVkIjp0cnVlLCJrZXkiOiJ0b2tlbiIsInZhbHVlIjoiZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SjFjMlZ5SWpwN0ltbGtJam9pTldFM1pUZzFOREF5WlRjeE56WXhNakZsTWpSaE1UazFJbjBzSW1saGRDSTZNVFV4T0RJME1UQTVOQ3dpWlhod0lqb3hOVEU0TWpRME5qazBmUS5XcVlGV1RILThXNWtqZFpuQVNfb1ZuZkxMTmNoVFlsWFkzREJXQ2NZYkRzIiwidHlwZSI6InRleHQifSx7ImVuYWJsZWQiOnRydWUsImtleSI6Imxlc3NvbklkIiwidmFsdWUiOiI1YTdlODU0YzJlNzE3NjEyMWUyNGExOTYiLCJ0eXBlIjoidGV4dCJ9XQ==)



### TODO
- complete implementing the API with the admin UI
- implementing the API with the admin UI
- writing tests
- complete Swagger DOCs