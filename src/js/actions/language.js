import * as types from '../constants/action-types'
import * as Language from '../models/language'

export const submitNewLanguage = (data) => {
    return (dispatch) => {
        dispatch({
            type: types.SUBMIT_NEW_LANGUAGE,
        })

        return Language.create(data)
            .then(
                (res) => {
                    dispatch(
                        {
                            type: types.NEW_LANGUAGE_BROADCAST,
                            payload: {
                                broadcast: 'REDIRECT'
                            }
                        }
                    )

                    return dispatch(
                        {
                            type: types.NEW_LANGUAGE_SUBMITTED,
                        }
                    )
                },
                (error) => {
                    const errors = error.response.data.error.errors
                    const inputsWithError = errors && Object.keys(errors) || []

                    return dispatch({
                        type: types.NEW_LANGUAGE_SUBMITION_FAILED,
                        payload: {
                            inputsWithError
                        }
                    })
                }
            )
    }
}

export const languageBroadcast = (broadcast) => {
    return {
        type: types.NEW_LANGUAGE_BROADCAST,
        payload: {
            broadcast
        }
    }
}
