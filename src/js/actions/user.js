import * as actionTypes from '../constants/action-types'

export const submitLogin = (data) => {
    return (dispatch) => {

        // trigger login submit
        dispatch({
            type: actionTypes.LOGIN_SUBMIT,
        })

        const request = $.ajax({
            url: 'http://127.0.0.1:3000/api/user/authenticate',
            contentType: 'application/json',
            type: 'POST',
            data: JSON.stringify({
                ...data
            })
        })

        // sending the authentication request and dispatch what happened then
        return request.then(
            (res) => {
                const { token } = res
                const { localStorage } = window

                localStorage.setItem('token', token)

                return dispatch({
                    type: actionTypes.GOT_TOKEN,
                })
            }, (error) => {
                dispatch({
                    type: actionTypes.LOGIN_SUBMITION_FAILED,
                    payload: {
                        ...error.responseJSON
                    }
                })
            }
        )
    }

}

export const checkToken = () => {
    const { localStorage } = window

    return {
        type: actionTypes.CHECK_TOKEN,
        payload: {
            isLogged: !!localStorage.getItem('token')
        }
    }
}

export const deleteToken = () => {
    const { localStorage } = window

    localStorage.removeItem('token')

    return {
        type: actionTypes.DELETE_TOKEN
    }
}

