import axios from 'axios'

// this is temporary
const url = 'http://127.0.0.1:3000/api/language'

const authorizedAxios = () => {
    const { localStorage } = window
    const token = localStorage.getItem('token')

    return axios.create({
        baseURL: url,
        headers: {'Authorization': `JWT ${token}`}
    })
}
export const read = () => {
    return axios({
        method: 'GET',
        url,
        responseType: 'json',
    })
}

export const create = (data) => {
    return authorizedAxios()
        .post('/', data)
}
