import React from 'react'
import ReactDOM from 'react-dom'

import { createStore, combineReducers, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import ReduxThunk from 'redux-thunk'

import createHistory from 'history/createBrowserHistory'
import {
    ConnectedRouter,
    routerReducer,
    routerMiddleware
} from 'react-router-redux'

import userReducer from './reducers/user'
import languageReducer from './reducers/language'

import { checkToken } from './actions/user'

import Root from './containers/root'

import '../sass/index.scss'

const history = createHistory()
const middleware = routerMiddleware(history)

const store = createStore(
    combineReducers(
        {
            router: routerReducer,
            user: userReducer,
            language: languageReducer,
        }
    ),
    applyMiddleware(
        middleware,
        ReduxThunk
    )
)

store.dispatch(
    checkToken()
)

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Root />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
)

