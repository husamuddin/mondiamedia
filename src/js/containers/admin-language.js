import { connect } from 'react-redux'
import language from '../components/admin-language'
import { submitNewLanguage, languageBroadcast } from '../actions/language'

export default connect(
    (state) => {
        const { language } = state

        return {
            language
        }
    },
    {
        submitNewLanguage,
        languageBroadcast
    }
)(language)
