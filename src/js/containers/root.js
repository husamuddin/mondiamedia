import { connect } from 'react-redux'
import Root from '../components/root'
import { checkToken } from '../actions/user'

export default connect(
    (state) => {
        const { user } = state

        return {
            user
        }
    }, {
        checkToken
    }
)(Root)

