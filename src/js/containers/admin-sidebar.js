import Sidebar from '../components/admin-sidebar'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    const { router: { location: { pathname } } } = state

    return {
        pathname
    }
}

export default connect(mapStateToProps)(Sidebar)

