import { connect } from 'react-redux'
import Admin from '../components/admin'
import { deleteToken } from '../actions/user'

export default connect(
    state => state,
    {
        deleteToken
    }
)(Admin)
