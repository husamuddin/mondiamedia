import { connect } from 'react-redux'
import Login from '../components/login'
import { submitLogin } from '../actions/user'

const mapStateToMaps = () => {

}

export default connect(
    (state) => {
        const { user } = state
        return {
            user
        }
    }, {
        submitLogin
    }
)(Login)

