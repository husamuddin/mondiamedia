import * as types from '../constants/action-types'

const defaultState = {
    errors: [],
    busy: false,
    isLogged: false,
}

export default (state = defaultState, action) => {
    const { type, payload } = action

    switch(type) {
        case types.LOGIN_SUBMIT:
            return {
                ...state,
                errors: [],
                busy: true,
            }
        case types.GOT_TOKEN:
            return {
                ...state,
                busy: false,
                isLogged: true,
            }
        case types.LOGIN_SUBMITION_FAILED:
            return {
                ...state,
                errors: [ payload.error ],
                busy: false,
            }
        case types.CHECK_TOKEN:
            const { isLogged } = payload

            return {
                ...state,
                isLogged: isLogged
            }
        case types.DELETE_TOKEN:
            return {
                ...state,
                isLogged: false
            }
        default:
            return {
                ...state
            }
    }

    return state
}
