import * as types from '../constants/action-types'

const defaultState = {
    errors: [],
    busy: false,
    broadcast: null,
}

export default (state = defaultState, action) => {
    const { payload, type } = action

    switch(type) {
        case types.SUBMIT_NEW_LANGUAGE:
            return {
                ...state,
                busy: true,
                errors: [],
            }

        case types.NEW_LANGUAGE_SUBMITTED:
            return {
                ...state,
                busy: false,
            }

        case types.NEW_LANGUAGE_SUBMITION_FAILED:
            return {
                ...state,
                busy: false,
                errors: payload.inputsWithError
            }

        case types.NEW_LANGUAGE_BROADCAST:
            return {
                ...state,
                broadcast: payload.broadcast
            }
        default:
            return {
                ...state
            }
    }
}
