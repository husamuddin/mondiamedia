import React from 'react'
import { Link } from 'react-router-dom'
import Header from './header'
import Sidebar from './sidebar'

export default () => {
    return (
        <div>
            <Header />
            <Sidebar />
        </div>
    )
}
