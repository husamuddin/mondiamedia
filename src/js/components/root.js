import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'


import Home from './home'
import Lessons  from './lessons'
import Admin from '../containers/admin'
import Login from '../containers/login'

export default class extends Component {
    componentWillMount() {
        const { checkToken } = this.props
    }

    render() {
        const { user, checkToken } = this.props
        const { isLogged } = user

        console.log('isLogin', isLogged)

        return (
            <Switch>
                <Route
                    exact path="/"
                    component={Home} />

                <Route
                    path="/admin"
                    render={
                        () => {
                            if(!isLogged) {
                                return (
                                    <Redirect to="/login" />
                                )
                            }

                            return (
                                <Admin />
                            )
                        }
                    } />

                <Route
                    path="/login"
                    component={Login} />

                <Route
                    path="/:language"
                    component={Lessons} />

            </Switch>
        )
    }
}
