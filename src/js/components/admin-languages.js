import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default class extends Component {

    componentWillMount() {
        const params = (
            (params = {})=> {
                const search = params.search

                if(
                    /^\?/i.test(search)
                ) {
                    return (
                        (pairs) => {
                            const params = {}

                            pairs
                                .forEach(
                                (pair) => {
                                    for (const key in pair) {
                                        params[key] = isNaN(pair[key]) ?
                                            pair[key] :
                                            +pair[key]

                                    }
                                }
                            )

                            return params
                        }
                    ) (
                        search.slice(1).split('&')
                            .map(
                                (pairStr) => {
                                    const pair = pairStr.split('=')

                                    return {
                                        [pair[0]]: pair[1] || null,
                                    }
                                }
                            )
                            .filter(
                                pair => {
                                    return !!Object.keys(pair)[0]
                                }
                            )
                    )
                }

                return {}
            }
        )(this.props.location)

        this.params = params
    }

    render() {
        return (
            <div>
                {
                    this.params['code'] && this.params['code'] == 1 && (
                        <div className="alert alert-success">
                            new language was added
                        </div>
                    )
                }
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1 className="h2">Languages</h1>
                    <div className="btn-toolbar">
                        <div className="btn-group">
                            <Link className="btn btn-secondary" to="/admin/languages/new">Add language</Link>
                        </div>
                    </div>
                </div>
                <div>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item active">
                                <Link to="/admin">Dashboard</Link>
                            </li>
                            <li className="breadcrumb-item active">Languages</li>
                        </ol>
                    </nav>

                    <nav className="d-flex justify-content-end">
                        <ul className="pagination">
                            <li className="page-item"><a className="page-link" href="#">Previous</a></li>
                            <li className="page-item"><a className="page-link" href="#">1</a></li>
                            <li className="page-item"><a className="page-link" href="#">2</a></li>
                            <li className="page-item"><a className="page-link" href="#">3</a></li>
                            <li className="page-item"><a className="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>

                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    name
                                </th>
                                <th>
                                    title
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                [1,2,3].map(
                                    (value, i) => (
                                        <tr key={i.toString()}>
                                            <td>
                                                <a href="#">
                                                    en3n2n4b52m23mnn2m4n234k2
                                                </a>
                                            </td>
                                            <td>
                                                java
                                            </td>
                                            <td>
                                                Java programming language
                                            </td>
                                        </tr>
                                    )
                                )
                            }
                        </tbody>
                    </table>


                    <nav className="d-flex justify-content-end">
                        <ul className="pagination">
                            <li className="page-item"><a className="page-link" href="#">Previous</a></li>
                            <li className="page-item"><a className="page-link" href="#">1</a></li>
                            <li className="page-item"><a className="page-link" href="#">2</a></li>
                            <li className="page-item"><a className="page-link" href="#">3</a></li>
                            <li className="page-item"><a className="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        )
    }
}
