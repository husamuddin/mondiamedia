import React from 'react'

export default() => {
    return (
        <div className="d-flex flex-column justify-content-center align-items-center" id="page-not-found">
            <h1 className="mb-4">PAGE NOT FOUND</h1>
            <iframe src="https://giphy.com/embed/NTXqH1bUCfHBS" width="480" height="322" frameBorder="0" className="giphy-embed" allowFullScreen></iframe>
        </div>
    )
}
