import React from 'react'
import { Link } from 'react-router-dom'

export default () => {
    return (
        <div>
            <div>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1 className="h2">Create lesson</h1>
                    <div className="btn-toolbar">
                        <div className="btn-group">
                            <button className="btn btn-secondary mr-2">Save</button>
                            <button className="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <nav aria-label="breadcrumb">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item active">
                            <Link to="/admin/lessons">Lessons</Link>
                        </li>
                        <li className="breadcrumb-item active">Create</li>
                    </ol>
                </nav>
            </div>
            <form action="">
                <div className="row mb-4">
                    <div className="col-md-6 mb-3">
                        <input
                            type="text"
                            placeholder="lesson name"
                            className="form-control" />
                    </div>
                    <div className="col-md-6 mb-3">
                        <input
                            type="text"
                            placeholder="lesson title"
                            className="form-control" />
                    </div>
                    <div className="col-md-12">
                        <textarea
                            id=""
                            name=""
                            cols="30"
                            rows="10"
                            placeholder="description"
                            className="form-control"
                            ></textarea>
                    </div>
                </div>
                <hr className="mb-4" />
                <div>
                    <p className="d-flex flex-row align-items-center">
                        <span className="mr-2" >
                            Adding examples
                        </span>
                        <a className="h3 my-0" href="#">
                            <i className="fas fa-plus-circle"></i>
                        </a>
                    </p>
                </div>
            </form>
        </div>
    )
}
