import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'

export default class extends Component {
    constructor(props) {
        super(props)
        this.handleSubmition = this.handleSubmition.bind(this)
    }

    handleSubmition(e) {
        e.preventDefault()

        const { history: {push} } = this.props

        const {
            name,
            title,
            description,
            introduction,
            example
        } = e.target

        const data = {
            name: name.value,
            title: title.value,
            description: description.value,
            introduction: introduction.value,
            example: example.value
        }

        this.props.submitNewLanguage(data)
    }

    componentWillUpdate(props) {
        if(props.language.broadcast === 'REDIRECT') {
            props.languageBroadcast(null)
        }
    }

    render() {
        const { language: {errors}, language: {busy} } = this.props
        const done = this.props.language.broadcast === 'REDIRECT'

        return !done && (
            <form onSubmit={this.handleSubmition}>
                <fieldset
                    {
                        ...(
                            busy && {
                                disabled: 'disabled'
                            } || {}
                        )
                    }
                    >
                    <div>
                        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                            <h1 className="h2">Create Language</h1>
                            <div className="btn-toolbar">
                                <div className="btn-group">
                                    <button className="btn btn-secondary mr-2">Save</button>
                                    <button className="btn btn-danger">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item active">
                                    <Link to="/admin/languages">Languages</Link>
                                </li>
                                <li className="breadcrumb-item active">Create</li>
                            </ol>
                        </nav>
                    </div>
                    <div>
                        <div className="row mb-4">
                            <div className="col-md-6 mb-3">
                                <input
                                    type="text"
                                    name="name"
                                    placeholder="language name"
                                    className={
                                        ((name) => {
                                            const classes = [ 'form-control' ]
                                            if(errors.indexOf(name) !== -1) {
                                                classes.push('is-invalid')
                                            }

                                            return classes.join(' ')
                                        })('name')
                                    } />
                            </div>
                            <div className="col-md-6 mb-3">
                                <input
                                    type="text"
                                    name="title"
                                    placeholder="language title"
                                    className={
                                        ((name) => {
                                            const classes = [ 'form-control' ]
                                            if(errors.indexOf(name) !== -1) {
                                                classes.push('is-invalid')
                                            }

                                            return classes.join(' ')
                                        })('title')
                                    } />
                            </div>
                            <div className="col-md-12 mb-3">
                                <textarea
                                    name="description"
                                    cols="30"
                                    rows="2"
                                    placeholder="description"
                                    className={
                                        ((name) => {
                                            const classes = [ 'form-control' ]
                                            if(errors.indexOf(name) !== -1) {
                                                classes.push('is-invalid')
                                            }

                                            return classes.join(' ')
                                        })('description')
                                    }
                                ></textarea>
                            </div>
                            <div className="col-md-12 mb-3">
                                <textarea
                                    name="introduction"
                                    cols="30"
                                    rows="10"
                                    placeholder="Introduction"
                                    className={
                                        ((name) => {
                                            const classes = [ 'form-control' ]
                                            if(errors.indexOf(name) !== -1) {
                                                classes.push('is-invalid')
                                            }

                                            return classes.join(' ')
                                        })('introduction')
                                    }
                                ></textarea>
                            </div>
                            <div className="col-md-12">
                                <textarea
                                    name="example"
                                    cols="30"
                                    rows="5"
                                    placeholder="example"
                                    className={
                                        ((name) => {
                                            const classes = [ 'form-control' ]
                                            if(errors.indexOf(name) !== -1) {
                                                classes.push('is-invalid')
                                            }

                                            return classes.join(' ')
                                        })('example')
                                    }
                                ></textarea>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        ) || (
            <Redirect to="/admin/languages?code=1" />
        )
    }
}
