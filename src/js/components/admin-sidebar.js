import React from 'react'
import { Link } from 'react-router-dom'

export default (props) => {
    const { pathname } = props

    const isDashboard = !!pathname.match(/^\/admin\/dashboard/)
    const isLanguage = !!pathname.match(/^\/admin\/languages/)
    const isLesson = !!pathname.match(/^\/admin\/lessons/)

    return (
        <aside className="col-md-2 d-none d-md-block bg-light sidebar">
            <div className="sidebar-sticky">
                <ul className="nav flex-column">
                    <li className="nav-item">
                        <Link className={ `nav-link ${isDashboard ? 'active' :''}` } to="/admin/dashboard">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                            Dashboard
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link className={ `nav-link ${isLanguage ? 'active' :''}` } to="/admin/languages">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
                            Languages
                        </Link>
                        <nav className="nav flex-column">
                            <Link className={ `nav-link ${pathname === '/admin/languages' ? 'active' :''}` } to="/admin/languages">List languages</Link>
                            <Link className={ `nav-link ${pathname === '/admin/languages/new' ? 'active' :''}` } to="/admin/languages/new">Create language</Link>
                        </nav>
                    </li>
                    <li className="nav-item">
                        <Link className={ `nav-link ${isLesson ? 'active' :''}` } to="/admin/lessons">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
                            Lessons
                        </Link>
                        <nav className="nav flex-column">
                            <Link className={ `nav-link ${pathname === '/admin/lessons' ? 'active' :''}` } to="/admin/lessons">List Lessons</Link>
                            <Link className={ `nav-link ${pathname === '/admin/lessons/new' ? 'active' :''}` } to="/admin/lessons/new">Create Lesson</Link>
                        </nav>
                    </li>
                </ul>
            </div>
        </aside>
    )
}


