import React from 'react'
import { Link } from 'react-router-dom'

export default (props) => {
    return (
        <aside className="col-md-2 d-none d-md-block bg-light sidebar">
            <div className="sidebar-sticky">
                <ul className="nav flex-column">
                    <li className="nav-item">
                        <Link className="nav-link" to="/html">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                            HTMl Introduction
                        </Link>
                    </li>
                    <li className="nav-item">
                        <span className="nav-link" to="/admin/languages">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
                            Lessons
                        </span>
                        <nav className="nav flex-column">
                            {
                                [...Array(20).keys()].map(
                                    (item, i) => (
                                        <Link key={i.toString()} className="nav-link" to="/html/doctype">
                                            Lorem Ipsum
                                        </Link>
                                    )
                                )
                            }
                        </nav>
                    </li>
                </ul>
            </div>
        </aside>
    )
}


