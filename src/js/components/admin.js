import React from 'react'
import { Route, Redirect, Switch } from 'react-router-dom'

import Login from './login'
import Header from './admin-header'
import Sidebar from '../containers/admin-sidebar'
import Dashboard from './admin-dashboard'
import PageNotFound from './admin-page-not-found'
import Languages from './admin-languages'
import Language from '../containers/admin-language'
import Lessons from './admin-lessons'
import Lesson from './admin-lesson'

export default (props) => {
    const { deleteToken } = props

    return (
        <div>
            <Route path="/admin" render={
                () => {
                    return (
                        <div id="dashboard">
                            <Header deleteToken={deleteToken} />
                            <div className="container-fluid">
                                <div className="row">
                                    <Sidebar />
                                    <main role="main" className="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
                                        <Switch>
                                            <Route
                                                exact
                                                path="/admin"
                                                render={
                                                    () => (
                                                        <Redirect
                                                            to="/admin/dashboard"
                                                        />
                                                    )
                                                } />

                                            <Route
                                                exact
                                                path="/admin/dashboard"
                                                component={Dashboard} />

                                            <Route
                                                exact
                                                path="/admin/languages"
                                                component={Languages} />

                                            <Route
                                                exact
                                                path="/admin/languages/new"
                                                component={Language} />

                                            <Route
                                                exact
                                                path="/admin/lessons"
                                                component={Lessons} />

                                            <Route
                                                exact
                                                path="/admin/lessons/new"
                                                component={Lesson} />

                                            <Route
                                                component={PageNotFound}
                                            />
                                        </Switch>
                                    </main>
                                </div>
                            </div>
                        </div>
                    )
                }
            } />
        </div>
    )
}
