import React from 'react'
import { Link } from 'react-router-dom'

export default () => {
    return (
        <div>
            <div>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                    <h1 className="h2">Lessons</h1>
                    <div className="btn-toolbar">
                        <div className="btn-group">
                            <Link className="btn btn-secondary" to="/admin/lessons/new">Add Lesson</Link>
                        </div>
                    </div>
                </div>
                <div>
                    <nav aria-label="breadcrumb">
                        <ol className="breadcrumb">
                            <li className="breadcrumb-item active">Lessons</li>
                        </ol>
                    </nav>

                    <nav className="d-flex justify-content-end">
                        <ul className="pagination">
                            <li className="page-item"><a className="page-link" href="#">Previous</a></li>
                            <li className="page-item"><a className="page-link" href="#">1</a></li>
                            <li className="page-item"><a className="page-link" href="#">2</a></li>
                            <li className="page-item"><a className="page-link" href="#">3</a></li>
                            <li className="page-item"><a className="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>

                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    name
                                </th>
                                <th>
                                    description
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                [1,2,3].map(
                                    (value, i) => (
                                        <tr key={i.toString()}>
                                            <td>
                                                <a href="#">
                                                    en3n2n4b52m23mnn2m4n234k2
                                                </a>
                                            </td>
                                            <td>
                                                java
                                            </td>
                                            <td>
                                                Java programming Lesson
                                            </td>
                                        </tr>
                                    )
                                )
                            }
                        </tbody>
                    </table>


                    <nav className="d-flex justify-content-end">
                        <ul className="pagination">
                            <li className="page-item"><a className="page-link" href="#">Previous</a></li>
                            <li className="page-item"><a className="page-link" href="#">1</a></li>
                            <li className="page-item"><a className="page-link" href="#">2</a></li>
                            <li className="page-item"><a className="page-link" href="#">3</a></li>
                            <li className="page-item"><a className="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    )
}
