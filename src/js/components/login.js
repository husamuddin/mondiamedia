import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'

export default class extends Component {
    constructor(props) {
        super(props)

        this.handleSubmition = this.handleSubmition.bind(this)
    }

    handleSubmition(e) {
        e.preventDefault()

        const usernameInput = e.target.username
        const passwordInput = e.target.password

        const username = usernameInput.value
        const password = passwordInput.value

        this.props.submitLogin({username, password})
    }

    render() {
        const { user } = this.props
        const errors = !!user.errors.length && user.errors
        const { busy, isLogged } = user

        return !isLogged && (
            <div id="login" className="text-center d-flex flex-column justify-content-center">
                <h1 className="display display-4 text-uppercase">
                    Mondimedia
                </h1>
                <form className="form-signin" onSubmit={this.handleSubmition}>
                    {
                        errors && (
                            <div className="alert alert-warning text-left">
                                <ul className="my-0">
                                    {
                                        errors.map(
                                            (error, i) => (
                                                <li key={i.toString()}>{ error.message }</li>
                                            )
                                        )
                                    }
                                </ul>
                            </div>
                        )
                    }

                    <fieldset {
                        ...(
                            (busy && {disabled: 'disabled'}) || {}
                        )
                    } >
                        <p className="mb-3 font-weight-normal">Please sign in</p>

                        <label htmlFor="username" className="sr-only">Email address</label>
                        <input name="username" type="text" id="username" className="form-control" placeholder="Email address" required="" autoFocus="" />

                        <label htmlFor="password" className="sr-only">Password</label>
                        <input name="password" type="password" id="password" className="form-control" placeholder="Password" required="" />

                        <button className="btn btn-lg btn-primary btn-block" type="submit">
                            {
                                (!busy && 'Sign in') || (
                                    <span className="fas fa-spinner fa-pulse"></span>
                                )
                            }
                        </button>
                    </fieldset>
                </form>
                <Link to="/">Back to home</Link>
            </div>
        ) || (
            <Redirect to="/admin" />
        )
    }
}
