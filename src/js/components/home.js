import React from 'react'
import { Link } from 'react-router-dom'
import {default as Highlight} from 'react-syntax-highlight'
import Header from './header'

export default () => {
    return (
        <div id="frontend">
            <Header />
            <div className="jumbotron">
                <div className="container text-center">
                    <h1 className="text-uppercase display-4">
                        Mondimedia <br />
                        <span className="display-2">
                            Task
                        </span>
                    </h1>
                    <blockquote>
                        <code>
                            { `<!-- The Educational Platform -->` }
                        </code>
                        </blockquote>
                </div>
            </div>

            <div id="languages">
                <div className="content">
                    <div className="articles">
                        {
                            [1, 2, 3].map(
                                (item, i) => (
                                    <article key={i.toString()} className="py-5">
                                        <div className="container">
                                            <div className="row">
                                                <div className="col-md-6 text-center">
                                                    <h3 className="display display-4">HTML</h3>
                                                    <p>
                                                        The language for building web pages
                                                    </p>
                                                    <div>
                                                        <Link to="/html" className="btn btn-secondary">
                                                            Browse Lessons
                                                        </Link>
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                    <p>HTML example</p>
                                                    <Highlight
                                                        lang={'html'}
                                                        value={`\
<!DOCTYPE html>
<html>
    <title>HTML Tutorial</title>
<body>

    <h1>This is a heading</h1>
    <p>This is a paragraph.</p>
    <script>
        var message = 'hello';
    </script>

</body>
</html>`}                                               />
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                )
                            )
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}
