import React from 'react'
import { Link } from 'react-router-dom'

export default () => {
    return (
        <nav className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
            <Link className="navbar-brand col-sm-3 col-md-2 mr-0" to="/">MondiaMedia</Link>
            <input
                className="form-control form-control-dark w-100"
                type="text" placeholder="Search"
                aria-label="Search" />
        </nav>
    )
}
